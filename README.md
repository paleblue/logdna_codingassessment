# LogDNA FrontEnd Engineer Challenge

Code for this challenge is found here: https://gitlab.com/paleblue/logdna_codingassessment

## Downloading

```console
> git clone https://gitlab.com/paleblue/logdna_codingassessment.git
```

## Using

**Note**: this has been developed and tested with React v16.13.2. YMMV with other versions. It won't matter with `docker-compose` scripts but might with running locally or linting/testing.

### Running

```console
> docker-compose up --build
```

Visit http://localhost:3000 in a browser.

### Local frontend

```console
> cd frontend
> yarn install
> yarn lint
> yarn test:no-watch
> yarn dev
```

### Local backend

```console
> cd backend
> yarn install
> yarn lint
> yarn test
> yarn dev
```


**Timeframe:** We know you have a busy schedule, so please feel free to take the time you need. Some candidates take several days to get it back to us. That's okay! Let us know if you will need more than a week, we will work with you

**Languages:** Javascript

**Frameworks:** Vue or React preferred, but you can use what you like. (We use Vue at LogDNA, but also have several people proficient in React)

**Tests:** very nice to have, but not mandatory

**Docs:** please provide enough documentation to get your app running smoothly. Any other details would be nice, but not manditory.

## Exercise: Building an Alert form

Part of the functionality of LogDNA is to alert users when certain events happen. An example would be if a server logged an API response status of 500, then we would send an email to a user.

Create a form that captures an Alert Message, Frequency for when an alert should be sent, and a list of Recipients.

![sample form layout](https://i.imgur.com/ZFXrMI1.png)

Feel free to use a platform like https://stackblitz.com/ to avoid setting up everything by yourself.

### Your form will need:

**Frequency.** You should be able to specify when you want to get your alerts.
only two values are valid: daily, hourly.

**Alert Message.** You should be able to add an alert message.

- The minimum length is 1 character.
- The maximum length is 1000 characters.

**Recipients.** You should be able to add and delete email recipients

- Recipients must be valid email addresses.
- The minimum number of recipients is 1.
- The maximum number of recipients is 10.

**Test Button.** When you click test button your application should hit test alert endpoint that will console out alert details.

**Save Button.** By clicking save button your application should hit save endpoint that will console out alert details.

After saving, the form should be cleared out and ready for new data to be entered. We are looking for:

- How you handle client-side validation.
- How and when you present error messages.
- How you manage state on the client-side.
- How you organize directories, components, APIs, and CSS.

### Your Server and REST API will need two endpoints:

- One for test alert button that will receive alert details and console them out.
- One for save button that will receive receive alert details and console them out.

There is no need to implement an email sender. There is no need to implement any storage mechanism. Use any framework you see fit. LogDNA's web application uses Express, but we also have Restify and Fastify in some repos.

#### We are looking for:

- How you handle server-side validation
- How you handle routing

#### We evaluate your ability to:

- Understand task scope
- Achieve production ready results
- Choose the right tools (packages, libraries, components, etc.)
- Organize folder/component structure that is easy to navigate
- Decompose the task into a set of subtasks
- Write maintainable and understandable code

#### We do not evaluate:

- Pixel perfectness. It should be clear effort was used to create a pleasant user experience, but we won’t be using a ruler ^\_^
- Features outside of scope
- Knowledge of quirks, hacks, non-standard behavior (no need for crossbrowser support, etc.) Memorized knowledge (feel free to use google, docs, stackoverflow, call a friend...) Knowledge of a particular package or library

## Always feel free to ask questions! Please email your recruiter

