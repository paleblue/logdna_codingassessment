'use strict';

const app = require('./server');
const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Alert config app listening at http://localhost:${port}`);
});

