'use strict';

const express = require('express');
const router = express.Router();
const { TOKEN, ARTIFICIAL_LATENCY } = require('../../constants');
const { checkSchema, validationResult } = require('express-validator');

const testAndSaveSchema = {
  token: {
    custom: {
      errorMessage: 'bad token',
      options: value => {
        // hard-coded for now, oh well!
        return value === TOKEN;
      },
    },
  },
  emails: {
    isArray: {
      errorMessage: 'Provide an array of between 1 and 10 email addresses',
      options: {
        min: 1,
        max: 10,
      },
    },
  },
  'emails.*': {
    isEmail: true,
  },
  message: {
    isLength: {
      errorMessage: 'Message must be between 1 and 1000 characters',
      options: {
        min: 1,
        max: 1000,
      },
    },
  },
  frequency: {
    custom: {
      errorMessage: 'Frequency must be either "daily" or "hourly"',
      options: value => {
        return ['daily','hourly'].includes(value);
      },
    },
  },
};

router.post('/test',
  checkSchema(testAndSaveSchema),
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      console.error('errors', errors.array());
      return res.status(400).json({ errors: errors.array() });
    } else {
      const successOutput = { success: true, action: 'test', message: '', timestamp: (new Date()).toGMTString(), data: { message: req.body.message, emails: req.body.emails, frequency: req.body.frequency } };
      console.log(successOutput);
      setTimeout(() => {
        res.json(successOutput);
      }, ARTIFICIAL_LATENCY);
    }
  },
);

router.post('/save',
  checkSchema(testAndSaveSchema),
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      console.error('errors', errors.array());
      return res.status(400).json({ errors: errors.array() });
    } else {
      const successOutput = { success: true, action: 'save', message: '', timestamp: (new Date()).toGMTString(), data: { message: req.body.message, emails: req.body.emails, frequency: req.body.frequency } };
      console.log(successOutput);
      setTimeout(() => {
        res.json(successOutput);
      }, ARTIFICIAL_LATENCY);
    }
  },
);

module.exports = router;
