'use strict';

const express = require('express');
const router = express.Router();
const { TOKEN, MIN_MESSAGE_LENGTH, MIN_EMAILS_LENGTH, MAX_MESSAGE_LENGTH, MAX_EMAILS_LENGTH } = require('../../constants');
const { checkSchema, validationResult } = require('express-validator');

const tokenCheckSchema = {
  token: {
    custom: {
      errorMessage: 'bad token',
      options: value => {
        // hard-coded for now, oh well!
        return value === TOKEN;
      },
    },
  },
};

router.post('/configuration',
  checkSchema(tokenCheckSchema),
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      console.error('errors', errors.array());
      return res.status(400).json({ errors: errors.array() });
    } else {
      const successOutput = {
        minMessageLength: MIN_MESSAGE_LENGTH,
        maxMessageLength: MAX_MESSAGE_LENGTH,
        minEmailsLength: MIN_EMAILS_LENGTH,
        maxEmailsLength: MAX_EMAILS_LENGTH,
      };
      return res.json(successOutput);
    }
  },
);

module.exports = router;
