'use strict';

const express = require('express');
const router = express.Router();
const { TOKEN, ARTIFICIAL_LATENCY } = require('../../constants');

router.post('/token',
  (req, res) => {
    // artifical latency only for assessment example
    setTimeout(() => {
      res.json({ token: TOKEN });
    }, ARTIFICIAL_LATENCY);
  },
);

module.exports = router;
