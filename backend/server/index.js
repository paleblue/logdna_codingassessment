'use strict';

const express = require('express');
const fs = require('fs');
const md = require('markdown-it')();
const alerts = require('./api/alerts');
const token = require('./api/token');
const configuration = require('./api/configuration');
const cors = require('cors');

const app = express();
app.use(express.json());
app.use(cors());

// also possibile to do this manually with a provided Origin,
// or by programmatically evaluating an origin, for better security:
// app.use((req, res, next) => {
  // res.header("Access-Control-Allow-Origin", process.env.CLIENT);
  // res.header("Access-Control-Allow-Headers", "Origin, Content-Type");
  // res.header("Access-Control-Allow-Methods", "GET, POST");
  // next();
// });

app.use('/api/v1', alerts);
app.use('/api/v1', token);
app.use('/api/v1', configuration);

app.get('/', (req, res) => {
  res.set('X-api-information', 'Access with POST to list API calls');
  const readMe = fs.readFileSync(__dirname + '/../../README.md', 'utf-8');
  const output = md.render(readMe);
  res.send(output);
});

app.post('/', (req, res) => {
  res.json({
    api: {
      token: '/api/v1/token',
      configuration: '/api/v1/configuration',
      test: '/api/v1/test',
      save: '/api/v1/save',
    },
  });
});

module.exports = app;
