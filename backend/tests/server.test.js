'use strict';

const app = require('../server');
const supertest = require('supertest');
const { TOKEN, MIN_MESSAGE_LENGTH, MAX_MESSAGE_LENGTH, MIN_EMAILS_LENGTH, MAX_EMAILS_LENGTH } = require('../constants');

console.error = jest.fn();
console.log = jest.fn();

test('GET /', async () => {
  await supertest(app)
    .get('/')
    .expect(200)
    .then(response => {
			expect(response.headers).toHaveProperty('x-api-information', 'Access with POST to list API calls');
      expect(response.text).toContain('Always feel free to ask questions!');
    });
});

test('POST /', async () => {
  await supertest(app)
    .post('/')
    .set('Accept', 'application/json')
    .expect(200)
    .then(response => {
      expect(response.body).toHaveProperty('api');
      expect(response.body.api).toHaveProperty('test','/api/v1/test');
      expect(response.body.api).toHaveProperty('save','/api/v1/save');
      expect(response.body.api).toHaveProperty('token','/api/v1/token');
    });
});

test('POST /api/v1/token', async () => {
  await supertest(app)
    .post('/api/v1/token')
    .set('Accept', 'application/json')
    .expect(200)
    .then(response => {
      expect(response.body).toHaveProperty('token');
      expect(response.body.token).toBe(TOKEN);
    });
});

describe('POST /api/v1/configuration', () => {
  test('/api/v1/configuration', async () => {
    await supertest(app)
      .post('/api/v1/configuration')
      .send({
        token: TOKEN,
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(200)
      .then(response => {
        expect(response.body).toEqual({
          minMessageLength: MIN_MESSAGE_LENGTH,
          maxMessageLength: MAX_MESSAGE_LENGTH,
          minEmailsLength: MIN_EMAILS_LENGTH,
          maxEmailsLength: MAX_EMAILS_LENGTH,
        });
      });
  });
  it('fails with missing token', async () => {
    const expectedErrors = [ { msg: 'bad token', param: 'token', location: 'body' } ];
    await supertest(app)
      .post('/api/v1/configuration')
      .send({
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
      });
  });

  it('fails with empty token', async () => {
    const expectedErrors = [ { msg: 'bad token', param: 'token', location: 'body', value: '' } ];
    await supertest(app)
      .post('/api/v1/configuration')
      .send({
        token: '',
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
      });
  });

  it('fails with bad token', async () => {
    const expectedErrors = [ { msg: 'bad token', param: 'token', location: 'body', value: 'abc' } ];
    await supertest(app)
      .post('/api/v1/configuration')
      .send({
        token: 'abc',
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
      });
  });
});

describe.each([
  { endpoint: 'test' },
  { endpoint: 'save' },
])('POST /api/v1/$endpoint', ({ endpoint }) => {
  test(`POST /api/v1/${endpoint}`, async () => {
    const expected = {
      success: true,
      message: '',
      action: endpoint,
      data: {
        message: 'def',
        frequency: 'daily',
        emails: [ 'test@test.com', 'test2@test.com', 'bob@test.com' ],
      },
    };
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        emails: expected.data.emails,
        message: expected.data.message,
        frequency: expected.data.frequency,
        token: TOKEN,
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(200)
      .then(response => {
        expect(response.body).toEqual(
          expect.objectContaining(expected),
        );
        expect(console.log).toHaveBeenCalledWith(
          expect.objectContaining(expected),
        );
      });
  });

  it('fails with missing token', async () => {
    const expectedErrors = [ { msg: 'bad token', param: 'token', location: 'body' } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        message: 'def',
        emails: [ 'test@test.com', 'test2@test.com', 'bob@test.com' ],
        frequency: 'daily',
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });

  it('fails with empty token', async () => {
    const expectedErrors = [ { msg: 'bad token', param: 'token', location: 'body', value: '' } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        message: 'def',
        emails: [ 'test@test.com', 'test2@test.com', 'bob@test.com' ],
        frequency: 'daily',
        token: '',
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });

  it('fails with bad token', async () => {
    const expectedErrors = [ { msg: 'bad token', param: 'token', location: 'body', value: 'abc' } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        message: 'def',
        emails: [ 'test@test.com', 'test2@test.com', 'bob@test.com' ],
        frequency: 'daily',
        token: 'abc',
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });

  it('fails with missing message', async () => {
    const expectedErrors = [ { msg: 'Message must be between 1 and 1000 characters', param: 'message', location: 'body' } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        emails: [ 'test@test.com', 'test2@test.com', 'bob@test.com' ],
        frequency: 'daily',
        token: TOKEN,
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });

  it('fails with empty message', async () => {
    const expectedErrors = [ { msg: 'Message must be between 1 and 1000 characters', param: 'message', location: 'body', value: '' } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        emails: [ 'test@test.com', 'test2@test.com', 'bob@test.com' ],
        message: '',
        frequency: 'daily',
        token: TOKEN,
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });

  it('fails with too large message', async () => {
    const largeText = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse id arcu leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla facilisi. Nullam rhoncus mattis enim, sed feugiat tellus pulvinar mattis. Sed imperdiet pellentesque ipsum ut luctus. Sed metus tortor, tincidunt non tempor ac, aliquet ut ligula. Ut ligula risus, maximus sed mi ac, viverra gravida orci. Proin fermentum, felis sit amet tincidunt eleifend, lorem mi iaculis erat, sed varius libero massa nec orci. Nunc lacinia tortor vehicula odio scelerisque varius. Integer a nisl et ipsum suscipit ultrices sit amet id ipsum.

Donec sed metus a orci egestas pellentesque. Fusce tristique gravida faucibus. Curabitur mattis, leo id egestas pretium, magna odio pharetra justo, nec placerat ligula lorem id velit. Aenean eget fringilla ex. Mauris quis laoreet sapien, a porttitor elit. Fusce et dui ut diam iaculis placerat. Integer pharetra nec purus eu imperdiet.

Nullam interdum laoreet nunc, ac porta justo sagittis at. Fusce justo nisi, euismod ac euismod ac, ornare eget quam. Quisque vel erat tincidunt, venenatis sem non, luctus mi. Maecenas quis interdum augue. Duis quis porttitor ante. Aliquam vitae orci euismod, gravida neque et, pretium metus. Proin feugiat urna ligula, a hendrerit libero porta eget. Nullam at elementum ipsum.

Donec vehicula urna vel maximus ultrices. Morbi sagittis dignissim leo eu vehicula. Aliquam pharetra porta sem ut dictum. Quisque tempor volutpat.`;
    const expectedErrors = [ { msg: 'Message must be between 1 and 1000 characters', param: 'message', location: 'body', value: largeText } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        emails: [ 'test@test.com', 'test2@test.com', 'bob@test.com' ],
        message: largeText,
        frequency: 'daily',
        token: TOKEN,
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });

  it('fails with missing frequency', async () => {
    const expectedErrors = [ { msg: 'Frequency must be either "daily" or "hourly"', param: 'frequency', location: 'body' } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        message: 'def',
        emails: [ 'test@test.com', 'test2@test.com', 'bob@test.com' ],
        token: TOKEN,
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });

  it('fails with empty frequency', async () => {
    const expectedErrors = [ { msg: 'Frequency must be either "daily" or "hourly"', param: 'frequency', location: 'body', value: '' } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        message: 'def',
        emails: [ 'test@test.com', 'test2@test.com', 'bob@test.com' ],
        frequency: '',
        token: TOKEN,
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });

  it('fails with bad frequency', async () => {
    const expectedErrors = [ { msg: 'Frequency must be either "daily" or "hourly"', param: 'frequency', location: 'body', value: 'weekly' } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        message: 'def',
        emails: [ 'test@test.com', 'test2@test.com', 'bob@test.com' ],
        frequency: 'weekly',
        token: TOKEN,
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });

  it('fails with missing emails list', async () => {
    const expectedErrors = [ { msg: 'Provide an array of between 1 and 10 email addresses', param: 'emails', location: 'body' } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        message: 'def',
        frequency: 'daily',
        token: TOKEN,
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });

  it('fails with empty emails list', async () => {
    const expectedErrors = [ { msg: 'Provide an array of between 1 and 10 email addresses', param: 'emails', location: 'body', value: [] } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        emails: [],
        message: 'def',
        frequency: 'daily',
        token: TOKEN,
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });

  it('fails with malformed emails list', async () => {
    const expectedErrors = [ { msg: 'Provide an array of between 1 and 10 email addresses', param: 'emails', location: 'body', value: 'not@array.com' } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        emails: 'not@array.com',
        message: 'def',
        frequency: 'daily',
        token: TOKEN,
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });

  it('fails with malformed email in list', async () => {
    const expectedErrors = [ { msg: 'Invalid value', param: 'emails[2]', location: 'body', value: 'bob' } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        emails: [ 'test@test.com', 'test2@test.com', 'bob' ],
        message: 'def',
        frequency: 'daily',
        token: TOKEN,
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });

  it('fails with too many emails', async () => {
    const manyEmails = [
      'test@test.com',
      'test2@test.com',
      'test3@test.com',
      'test4@test.com',
      'test5@test.com',
      'test6@test.com',
      'test7@test.com',
      'test8@test.com',
      'test9@test.com',
      'test10@test.com',
      'test11@test.com',
      'test12@test.com',
    ];
    const expectedErrors = [ { msg: 'Provide an array of between 1 and 10 email addresses', param: 'emails', location: 'body', value: manyEmails } ];
    await supertest(app)
      .post(`/api/v1/${endpoint}`)
      .send({
        emails: manyEmails,
        message: 'def',
        frequency: 'daily',
        token: TOKEN,
      })
      .set('Content-Type', 'application/json')
      .set('Accept', 'application/json')
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({ errors: expectedErrors });
        expect(console.error).toHaveBeenCalledWith('errors', expectedErrors);
      });
  });
});
