import { useEffect } from 'react';
import '@scss/App.scss';
import { Header, Spinner } from '@components';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { useDispatch, useSelector } from 'react-redux';
import { fetchToken, fetchConfiguration } from '@actions';

const App = ({ component, pageTitle }) => {
	const token = useSelector(state => state.token);
	const configuration = useSelector(state => state.configuration);
  const dispatch = useDispatch();
  
  const loading = Boolean(token.isLoading || configuration.isLoading);

	useEffect(() => {
    dispatch(fetchToken());
  }, [dispatch]);

  useEffect(() => {
    dispatch(fetchConfiguration());
  }, [dispatch, token.token]);

  return (
    <Box className="App">
      <Header pageTitle={pageTitle} />

      <Paper
        variation="elevation"
        elevation={4}
        sx={{
          display: 'flex',
          flexDirection: 'column',
          flexGrow: 1,
          background: 'var(--color-card-background)',
          width: '100%',
          maxWidth: 'var(--page-width)',
          padding: '48px',
      }}>
        { loading ? <Spinner /> : component }
      </Paper>
    </Box>
  );
};

export default App;
