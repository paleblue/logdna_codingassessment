import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from '@/App';
import { AlertManager } from '@containers';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from '@reducers';
import thunk from 'redux-thunk';
import { MemoryRouter, Routes, Route } from 'react-router-dom';

const _minMessageLength = 5;
const _maxMessageLength = 25;
const _minEmailsLength = 2;
const _maxEmailsLength = 4;

const _token = {token: '123456789asdf'};

const _urlToken = 'undefined:undefined/api/v1/token';
const _urlConfiguration = 'undefined:undefined/api/v1/configuration';
const _urlTest = 'undefined:undefined/api/v1/test';
const _urlSave = 'undefined:undefined/api/v1/save';

const _configuration = {
  minMessageLength: _minMessageLength,
  maxMessageLength: _maxMessageLength,
  minEmailsLength: _minEmailsLength,
  maxEmailsLength: _maxEmailsLength,
};

const _testData = {
  message: 'one two three four',
  frequency: 'hourly',
  emails: ['one@gmail.com', 'two@gmail.com'],
};

const _testSuccess = {
  success: true,
  action: 'test',
  message: '',
  data: _testData,
};

const _saveSuccess = {
  success: true,
  action: 'save',
  message: '',
  data: _testData,
};

const _testError = {
  success: false,
  action: 'test',
  message: '',
  data: {},
};

const _saveError = {
  success: false,
  action: 'save',
  message: '',
  data: {},
};

describe('basic app integration tests', () => {

  beforeEach(() => {
    fetch
      .mockImplementation((u, ...rest) => {
        if (u === _urlToken) {
          return Promise.resolve(new Response(JSON.stringify(_token)));
        } else if (u === _urlConfiguration) {
          return Promise.resolve(new Response(JSON.stringify(_configuration)));
        } else if (u === _urlTest) {
          return Promise.resolve(new Response(JSON.stringify(_testSuccess)));
        } else if (u === _urlSave) {
          return Promise.resolve(new Response(JSON.stringify(_saveSuccess)));
        }
        return Promise.resolve(new Response(''));
      })
    ;
  });

  afterEach(() => {
    fetch.resetMocks();
    jest.clearAllMocks();
  });

  const renderComponent = (child) => {
    const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));
    return render(
      <Provider store={store}>
        <MemoryRouter>
          <Routes>
            <Route path="*" element={<App component={<AlertManager/>} pageTitle="Alert Manager" />} />
          </Routes>
        </MemoryRouter>
      </Provider>,
    );
  };

  it('renders the header title', async () => {
    renderComponent(<App />);
    const headerElement = screen.getByText(/alert manager/i);
    expect(headerElement).toBeInTheDocument();
  });

  it('renders the form title, but only after token and configuration are received', async () => {
    renderComponent(<App />);
    const headerElement = screen.getByText(/alert manager/i);
    expect(headerElement).toBeInTheDocument();
  
    const formTitleElement = screen.queryByText(/email alert/i);
    expect(formTitleElement).not.toBeInTheDocument();

    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/token', expect.anything()));
    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/configuration', expect.anything()));

    await waitFor(() => {
      const headerElement = screen.getByText(/email alert/i);
      expect(headerElement).toBeInTheDocument();
    });

    await waitFor(() => {
      const noTokenElement = screen.queryByText(/error: no token/i);
      expect(noTokenElement).not.toBeInTheDocument();
    });

    await waitFor(() => {
      const noConfigElement = screen.queryByText(/error: no configuration/i);
      expect(noConfigElement).not.toBeInTheDocument();
    });
  });

  it('has disabled buttons when first loaded', async () => {
    renderComponent(<App />);
    const headerElement = screen.getByText(/alert manager/i);
    expect(headerElement).toBeInTheDocument();
  
    const formTitleElement = screen.queryByText(/email alert/i);
    expect(formTitleElement).not.toBeInTheDocument();

    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/token', expect.anything()));
    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/configuration', expect.anything()));

    await waitFor(() => {
      const headerElement = screen.getByText(/email alert/i);
      expect(headerElement).toBeInTheDocument();
    });

    await waitFor(() => {
      const testButton = screen.getByRole('button', { name: /test/i });
      expect(testButton).toHaveAttribute('disabled');
    });

    await waitFor(() => {
      const saveButton = screen.getByRole('button', { name: /save/i });
      expect(saveButton).toHaveAttribute('disabled');
    });
  });

  describe('message field tests', () => {
    const setupTestBlock = async () => {
      renderComponent(<App />);

      const emailsField = await screen.findByLabelText(/emails/i);
      userEvent.type(emailsField, 'one@gmail.com');
      userEvent.type(emailsField, '{enter}');
      userEvent.type(emailsField, 'two@gmail.com');
      userEvent.type(emailsField, '{enter}');
    };

    it('to have disabled buttons if message is omitted', async () => {
      await setupTestBlock();
      
      await waitFor(() => {
        const emailOneElement = screen.getByText(/one@gmail.com/i);
        expect(emailOneElement).toBeInTheDocument();
      });

      await waitFor(() => {
        const testButton = screen.getByRole('button', { name: /test/i });
        expect(testButton).toHaveAttribute('disabled');
      });

      await waitFor(() => {
        const saveButton = screen.getByRole('button', { name: /save/i });
        expect(saveButton).toHaveAttribute('disabled');
      });
    });

    it('to have enabled TEST button if all required fields are filled', async () => {
      await setupTestBlock();
      
      const messageField = screen.getByLabelText(/message/i);
      const shortMessage = 'abcdefghijklmnopqrstuvwxyz'.substr(0,_minMessageLength-1);
      userEvent.type(messageField, shortMessage);
      expect(messageField).toHaveValue(shortMessage);

      await waitFor(() => {
        const emailOneElement = screen.getByText(/one@gmail.com/i);
        expect(emailOneElement).toBeInTheDocument();
      });

      await waitFor(() => {
        const testButton = screen.getByRole('button', { name: /test/i });
        expect(testButton).not.toHaveAttribute('disabled');
      });

      await waitFor(() => {
        const saveButton = screen.getByRole('button', { name: /save/i });
        expect(saveButton).toHaveAttribute('disabled');
      });
    });

    it('not send TEST if message is too short', async () => {
      await setupTestBlock();
      
      const messageField = screen.getByLabelText(/message/i);
      const shortMessage = 'abcdefghijklmnopqrstuvwxyz'.substr(0,_minMessageLength-1);
      userEvent.type(messageField, shortMessage);
      expect(messageField).toHaveValue(shortMessage);

      const testButton = screen.getByRole('button', { name: /test/i });
      userEvent.click(testButton);

      expect(await screen.findByText(`message must be at least ${_minMessageLength} characters in length`)).toBeInTheDocument();
      await waitFor(() => expect(screen.queryByText(`message must be less than ${_maxMessageLength} characters in length`)).not.toBeInTheDocument());

      await waitFor(() => expect(fetch).not.toHaveBeenCalledWith('undefined:undefined/api/v1/test', expect.anything()));
    });

    it('not send TEST if message is too long', async () => {
      await setupTestBlock();
      
      const messageField = screen.getByLabelText(/message/i);
      const longMessage = 'abcdefghijklmnopqrstuvwxyz'.substr(0,_maxMessageLength+1);
      userEvent.type(messageField, longMessage);
      expect(messageField).toHaveValue(longMessage);

      const testButton = screen.getByRole('button', { name: /test/i });
      userEvent.click(testButton);

      await waitFor(() => expect(screen.queryByText(`message must be at least ${_minMessageLength} characters in length`)).not.toBeInTheDocument());
      expect(await screen.findByText(`message must be less than ${_maxMessageLength} characters in length`)).toBeInTheDocument();
      await waitFor(() => expect(fetch).not.toHaveBeenCalledWith('undefined:undefined/api/v1/test', expect.anything()));
    });
  });

  describe('email field tests', () => {
    const setupTestBlock = async () => {
      renderComponent(<App />);

      const messageField = await screen.findByLabelText(/message/i);
      const shortMessage = 'abcdefghijklmnopqrstuvwxyz'.substr(0,_minMessageLength-1);
      userEvent.type(messageField, shortMessage);
    };

    it('can have emails added with button', async () => {
      await setupTestBlock();

      const emailsField = screen.getByLabelText(/emails/i);
      userEvent.type(emailsField, 'one@gmail.com');
      expect(emailsField).toHaveValue('one@gmail.com');

      const addButton = screen.getByTestId('add-email-button');
      userEvent.click(addButton);
      await waitFor(() => expect(emailsField).toHaveValue(''));

      expect(screen.getByText(/one@gmail.com/i)).toBeInTheDocument();
    });

    it('can have emails added with enter key', async () => {
      await setupTestBlock();
      
      const emailsField = screen.getByLabelText(/emails/i);
      userEvent.type(emailsField, 'one@gmail.com');
      expect(emailsField).toHaveValue('one@gmail.com');
      userEvent.type(emailsField, '{enter}');
      await waitFor(() => expect(emailsField).toHaveValue(''));

      expect(screen.getByText(/one@gmail.com/i)).toBeInTheDocument();
    });

    it('errors on malformed email', async () => {
      await setupTestBlock();
      
      const emailsField = screen.getByLabelText(/emails/i);
      userEvent.type(emailsField, 'one@gmail.com');
      expect(emailsField).toHaveValue('one@gmail.com');
      userEvent.type(emailsField, '{enter}');
      await waitFor(() => expect(emailsField).toHaveValue(''));

      userEvent.type(emailsField, 'two@go');
      expect(emailsField).toHaveValue('two@go');
      userEvent.type(emailsField, '{enter}');
      expect(emailsField).toHaveValue('two@go');
      expect(screen.queryByText(/two@go/i)).not.toBeInTheDocument();

      const addButton = screen.getByTestId('add-email-button');
      expect(addButton).toHaveAttribute('disabled');
    });

    it('errors on duplicate emails', async () => {
      await setupTestBlock();
      
      const emailsField = screen.getByLabelText(/emails/i);
      userEvent.type(emailsField, 'one@gmail.com');
      expect(emailsField).toHaveValue('one@gmail.com');
      userEvent.type(emailsField, '{enter}');
      await waitFor(() => expect(emailsField).toHaveValue(''));

      userEvent.type(emailsField, 'one@gmail.com');
      expect(emailsField).toHaveValue('one@gmail.com');
      userEvent.type(emailsField, '{enter}');
      await waitFor(() => {
        const duplicateMessage = screen.getByText('duplicate email');
        expect(duplicateMessage).toBeInTheDocument();
      });

      expect(screen.getByText(/one@gmail.com/i)).toBeInTheDocument();
    });

    it('errors on too many emails', async () => {
      await setupTestBlock();
      
      const emailsField = screen.getByLabelText(/emails/i);
      userEvent.type(emailsField, 'one@gmail.com');
      expect(emailsField).toHaveValue('one@gmail.com');
      userEvent.type(emailsField, '{enter}');
      await waitFor(() => expect(emailsField).toHaveValue(''));
      userEvent.type(emailsField, 'two@gmail.com');
      expect(emailsField).toHaveValue('two@gmail.com');
      userEvent.type(emailsField, '{enter}');
      await waitFor(() => expect(emailsField).toHaveValue(''));
      userEvent.type(emailsField, 'three@gmail.com');
      expect(emailsField).toHaveValue('three@gmail.com');
      userEvent.type(emailsField, '{enter}');
      await waitFor(() => expect(emailsField).toHaveValue(''));
      userEvent.type(emailsField, 'four@gmail.com');
      expect(emailsField).toHaveValue('four@gmail.com');
      userEvent.type(emailsField, '{enter}');
      await waitFor(() => expect(emailsField).toHaveValue(''));
      userEvent.type(emailsField, 'five');
      expect(emailsField).toHaveValue('five');

      await waitFor(() => {
        const tooManyMessage = screen.getByText(`no more than ${_maxEmailsLength} emails allowed`);
        expect(tooManyMessage).toBeInTheDocument();
      });

      expect(screen.getByText(/one@gmail.com/i)).toBeInTheDocument();
    });

    it('to have disabled buttons if emails are omitted', async () => {
      await setupTestBlock();

      expect(screen.queryByText(/one@gmail.com/i)).not.toBeInTheDocument();

      await waitFor(() => {
        const testButton = screen.getByRole('button', { name: /test/i });
        expect(testButton).toHaveAttribute('disabled');
      });

      await waitFor(() => {
        const saveButton = screen.getByRole('button', { name: /save/i });
        expect(saveButton).toHaveAttribute('disabled');
      });
    });

    it('to have enabled TEST button if all required fields are filled', async () => {
      await setupTestBlock();
      
      const emailsField = screen.getByLabelText(/emails/i);
      userEvent.type(emailsField, 'one@gmail.com');
      expect(emailsField).toHaveValue('one@gmail.com');
      userEvent.type(emailsField, '{enter}');
      await waitFor(() => expect(emailsField).toHaveValue(''));

      expect(screen.getByText(/one@gmail.com/i)).toBeInTheDocument();

      await waitFor(() => {
        const testButton = screen.getByRole('button', { name: /test/i });
        expect(testButton).not.toHaveAttribute('disabled');
      });

      await waitFor(() => {
        const saveButton = screen.getByRole('button', { name: /save/i });
        expect(saveButton).toHaveAttribute('disabled');
      });
    });
  });

  describe('test and save calls', () => {
    const setupTestBlock = async () => {
      renderComponent(<App />);

      const emailsField = await screen.findByLabelText(/emails/i);
      userEvent.type(emailsField, 'one@gmail.com');
      userEvent.type(emailsField, '{enter}');
      userEvent.type(emailsField, 'two@gmail.com');
      userEvent.type(emailsField, '{enter}');

      const messageField = await screen.findByLabelText(/message/i);
      const goodMessage = 'abcdefghijklmnopqrstuvwxyz'.substr(0,_maxMessageLength-2);
      userEvent.type(messageField, goodMessage);
    };

    it('will send TEST if all parameters pass local validation', async () => {
      await setupTestBlock();
      
      const testButton = screen.getByRole('button', { name: /test/i });
      userEvent.click(testButton);

      await waitFor(() => {
        expect(testButton).toHaveAttribute('disabled');
      });

      await waitFor(() => expect(screen.queryByText(`message must be at least ${_minMessageLength} characters in length`)).not.toBeInTheDocument());
      await waitFor(() => expect(screen.queryByText(`message must be less than ${_maxMessageLength} characters in length`)).not.toBeInTheDocument());
      await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/test', expect.anything()));

      expect(await screen.findByText(/test was a success/i)).toBeInTheDocument();
    });

    it('will allow clicking SAVE after testing', async () => {
      await setupTestBlock();

      const testButton = screen.getByRole('button', { name: /test/i });
      const saveButton = screen.getByRole('button', { name: /save/i });

      await waitFor(() => {
        expect(saveButton).toHaveAttribute('disabled');
      });

      userEvent.click(testButton);
      await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/test', expect.anything()));


      await waitFor(() => {
        expect(saveButton).not.toHaveAttribute('disabled');
      });

      userEvent.click(saveButton);
      await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/save', expect.anything()));

      expect(await screen.findByText(/save was a success/i)).toBeInTheDocument();
    });

    it('will disable SAVE after something has changed', async () => {
      await setupTestBlock();

      const testButton = screen.getByRole('button', { name: /test/i });
      const saveButton = screen.getByRole('button', { name: /save/i });

      await waitFor(() => {
        expect(saveButton).toHaveAttribute('disabled');
      });

      userEvent.click(testButton);
      await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/test', expect.anything()));

      await waitFor(() => {
        expect(saveButton).not.toHaveAttribute('disabled');
      });

      const messageField = await screen.findByLabelText(/message/i);
      userEvent.type(messageField, 'd');

      await waitFor(() => {
        expect(saveButton).toHaveAttribute('disabled');
      });
    });
  });
});

describe('api failure tests', () => {

  const renderComponent = (child) => {
    const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));
    return render(
      <Provider store={store}>
        <MemoryRouter>
          <Routes>
            <Route path="*" element={<App component={<AlertManager/>} pageTitle="Alert Manager" />} />
          </Routes>
        </MemoryRouter>
      </Provider>,
    );
  };

  it('fails to render the form if token fails', async () => {
    fetch
      .mockImplementation((u, ...rest) => {
        if (u === _urlToken) {
          const errorResponse = new Response();
          errorResponse.status = 400;
          return Promise.resolve(errorResponse);
        }
      })
    ;

    renderComponent(<App />);
    const headerElement = screen.getByText(/alert manager/i);
    expect(headerElement).toBeInTheDocument();
  
    const formTitleElement = screen.queryByText(/email alert/i);
    expect(formTitleElement).not.toBeInTheDocument();

    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/token', expect.anything()));
    await waitFor(() => expect(fetch).not.toHaveBeenCalledWith('undefined:undefined/api/v1/configuration', expect.anything()));

    await waitFor(() => {
      const headerElement = screen.queryByText(/email alert/i);
      expect(headerElement).toBeInTheDocument();
    });

    await waitFor(() => {
      const noTokenElement = screen.queryByText(/error: no token/i);
      expect(noTokenElement).toBeInTheDocument();
    });
  });

  it('fails to render the form if configuration fails', async () => {
    fetch
      .mockImplementation((u, ...rest) => {
        if (u === _urlToken) {
          return Promise.resolve(new Response(JSON.stringify({token: '123456789asdf'})));
        } else if (u === _urlConfiguration) {
          const errorResponse = new Response();
          errorResponse.status = 400;
          return Promise.resolve(errorResponse);
        }
      })
    ;

    renderComponent(<App />);
    const headerElement = screen.getByText(/alert manager/i);
    expect(headerElement).toBeInTheDocument();
  
    const formTitleElement = screen.queryByText(/email alert/i);
    expect(formTitleElement).not.toBeInTheDocument();

    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/token', expect.anything()));
    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/configuration', expect.anything()));

    await waitFor(() => {
      const headerElement = screen.queryByText(/email alert/i);
      expect(headerElement).toBeInTheDocument();
    });

    await waitFor(() => {
      const noTokenElement = screen.queryByText(/error: no token/i);
      expect(noTokenElement).not.toBeInTheDocument();
    });

    await waitFor(() => {
      const noTokenElement = screen.queryByText(/error: no configuration/i);
      expect(noTokenElement).toBeInTheDocument();
    });
  });

  it('shows an error if TEST fails', async () => {
    fetch
      .mockImplementation((u, ...rest) => {
        if (u === _urlToken) {
          return Promise.resolve(new Response(JSON.stringify(_token)));
        } else if (u === _urlConfiguration) {
          return Promise.resolve(new Response(JSON.stringify(_configuration)));
        } else if (u === _urlTest) {
          return Promise.resolve(new Response(JSON.stringify(_testError)));
        }
        return Promise.resolve(new Response(''));
      })
    ;

    renderComponent(<App />);
    const headerElement = screen.getByText(/alert manager/i);
    expect(headerElement).toBeInTheDocument();
  
    const formTitleElement = screen.queryByText(/email alert/i);
    expect(formTitleElement).not.toBeInTheDocument();

    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/token', expect.anything()));
    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/configuration', expect.anything()));

    const emailsField = await screen.findByLabelText(/emails/i);
    userEvent.type(emailsField, 'one@gmail.com');
    expect(emailsField).toHaveValue('one@gmail.com');
    userEvent.type(emailsField, '{enter}');
    expect(emailsField).toHaveValue('');
    expect(screen.getByText(/one@gmail.com/i)).toBeInTheDocument();
    userEvent.type(emailsField, 'two@gmail.com');
    expect(emailsField).toHaveValue('two@gmail.com');
    userEvent.type(emailsField, '{enter}');
    expect(emailsField).toHaveValue('');
    expect(screen.getByText(/two@gmail.com/i)).toBeInTheDocument();

    const messageField = screen.getByLabelText(/message/i);
    const goodMessage = 'abcdefghijklmnopqrstuvwxyz'.substr(0,_maxMessageLength-2);
    userEvent.type(messageField, goodMessage);
    expect(messageField).toHaveValue(goodMessage);

    const testButton = screen.getByRole('button', { name: /test/i });
    userEvent.click(testButton);

    await waitFor(() => {
      expect(testButton).toHaveAttribute('disabled');
    });

    await waitFor(() => expect(screen.queryByText(`message must be at least ${_minMessageLength} characters in length`)).not.toBeInTheDocument());
    await waitFor(() => expect(screen.queryByText(`message must be less than ${_maxMessageLength} characters in length`)).not.toBeInTheDocument());
    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/test', expect.anything()));

    await waitFor(() => {
      const successMessage = screen.queryByText(/test was a success/i);
      expect(successMessage).not.toBeInTheDocument();
    });

    await waitFor(() => {
      const errorMessage = screen.getByText(/test error/i);
      expect(errorMessage).toBeInTheDocument();
    });
  });

  it('shows an error if SAVE fails', async () => {
    fetch
      .mockImplementation((u, ...rest) => {
        if (u === _urlToken) {
          return Promise.resolve(new Response(JSON.stringify(_token)));
        } else if (u === _urlConfiguration) {
          return Promise.resolve(new Response(JSON.stringify(_configuration)));
        } else if (u === _urlTest) {
          return Promise.resolve(new Response(JSON.stringify(_testSuccess)));
        } else if (u === _urlSave) {
          return Promise.resolve(new Response(JSON.stringify(_saveError)));
        }
        return Promise.resolve(new Response(''));
      })
    ;

    renderComponent(<App />);
    const headerElement = screen.getByText(/alert manager/i);
    expect(headerElement).toBeInTheDocument();
  
    const formTitleElement = screen.queryByText(/email alert/i);
    expect(formTitleElement).not.toBeInTheDocument();

    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/token', expect.anything()));
    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/configuration', expect.anything()));

    const emailsField = screen.getByLabelText(/emails/i);
    userEvent.type(emailsField, 'one@gmail.com');
    expect(emailsField).toHaveValue('one@gmail.com');
    userEvent.type(emailsField, '{enter}');
    expect(emailsField).toHaveValue('');
    expect(screen.getByText(/one@gmail.com/i)).toBeInTheDocument();
    userEvent.type(emailsField, 'two@gmail.com');
    expect(emailsField).toHaveValue('two@gmail.com');
    userEvent.type(emailsField, '{enter}');
    expect(emailsField).toHaveValue('');
    expect(screen.getByText(/two@gmail.com/i)).toBeInTheDocument();

    const messageField = screen.getByLabelText(/message/i);
    const goodMessage = 'abcdefghijklmnopqrstuvwxyz'.substr(0,_maxMessageLength-2);
    userEvent.type(messageField, goodMessage);
    expect(messageField).toHaveValue(goodMessage);

    const testButton = screen.getByRole('button', { name: /test/i });
    const saveButton = screen.getByRole('button', { name: /save/i });

    await waitFor(() => {
      expect(saveButton).toHaveAttribute('disabled');
    });

    userEvent.click(testButton);
    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/test', expect.anything()));

    await waitFor(() => {
      expect(saveButton).not.toHaveAttribute('disabled');
    });

    userEvent.click(saveButton);
    await waitFor(() => expect(fetch).toHaveBeenCalledWith('undefined:undefined/api/v1/save', expect.anything()));

    await waitFor(() => {
      const successMessage = screen.queryByText(/save was a success/i);
      expect(successMessage).not.toBeInTheDocument();
    });

    await waitFor(() => {
      const errorMessage = screen.getByText(/save error/i);
      expect(errorMessage).toBeInTheDocument();
    });
  });
});
