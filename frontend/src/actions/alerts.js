import { createAsyncThunk } from '@reduxjs/toolkit';

export const testAlert = createAsyncThunk(
  'alert/test',
  async (info, { getState }) => {
		const token = getState().token.token;
    if (!token) {
      throw new Error('failed to obtain token');
    }

		const response = await fetch(`${process.env.REACT_APP_API}:${process.env.REACT_APP_API_PORT}/api/v1/test`, {
			method: 'POST',
			crossDomain: true,
			headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        ...info,
        'token': token,
      }),
		}).then(data => data.json());
		return response;
  },
);

export const saveAlert = createAsyncThunk(
  'alert/save',
  async (info, { getState }) => {
		const token = getState().token.token;
    if (!token) {
      throw new Error('failed to obtain token');
    }

    const response = await fetch(`${process.env.REACT_APP_API}:${process.env.REACT_APP_API_PORT}/api/v1/save`, {
      method: 'POST',
      crossDomain: true,
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        ...info,
        'token': token,
      }),
    }).then(data => data.json());
    return response;
  },
);
