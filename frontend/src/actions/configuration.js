import { createAsyncThunk } from '@reduxjs/toolkit';

export const fetchConfiguration = createAsyncThunk(
  'configuration/fetch',
  async (_, { getState }) => {
		const token = getState().token.token;
    if (!token) {
      throw new Error('failed to obtain token');
    }

    const response = await fetch(`${process.env.REACT_APP_API}:${process.env.REACT_APP_API_PORT}/api/v1/configuration`, {
      method: 'POST',
      crossDomain: true,
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({'token': token}),
    }).then(data => data.json());
    return response;
  },
);
