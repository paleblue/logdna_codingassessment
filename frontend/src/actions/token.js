import { createAsyncThunk } from '@reduxjs/toolkit';

export const fetchToken = createAsyncThunk(
  'token/fetch',
  // eslint-disable-next-line no-empty-pattern
  async (_, { }) => {
    const response = await fetch(`${process.env.REACT_APP_API}:${process.env.REACT_APP_API_PORT}/api/v1/token`, {
      method: 'POST',
      crossDomain: true,
      headers: {'Content-Type': 'application/json'},
    }).then(data => data.json());
    return response;
  },
);
