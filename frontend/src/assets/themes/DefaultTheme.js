import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    background: {
      default: 'var(--color-background)',
			header: 'var(--color-header-background)',
    },
    text: {
      default: 'var(--color-background-text)',
      header: 'var(--color-header-text)',
    },
    primary: {
      main: '#DB0A5B',
      note: '#777',
    },
    secondary: {
      main: '#010037',
    },
  },
  components: {
    MuiFormControl: {
      styleOverrides: {
        root: {
          margin: '12px 0',
        },
      },
    },
    MuiPaper: {
      styleOverrides: {
        root: {
          backgroundColor: 'var(--color-card-background)',
          color: 'var(--color-card-text)',
        },
      },
    },
    MuiFormLabel: {
      defaultProps: {
        color: 'secondary',
      },
    },
    MuiRadio: {
      defaultProps: {
        color: 'secondary',
      },
    },
    MuiOutlinedInput: {
      defaultProps: {
        color: 'secondary',
        notched: false,
      },
      styleOverrides: {
        root: {
          marginTop: '2rem',
        },
      },
    },
    MuiInputLabel: {
      defaultProps: {
        shrink: true,
      },
      styleOverrides: {
        root: {
          transform: 'none',
        },
      },
    },
  },
});

export default theme;
