import { useState, useEffect, useCallback, useMemo, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { testAlert, saveAlert } from '@actions';
import { InputList } from '@components';
import { InputMinMaxLength } from '@components';
import Fade from '@mui/material/Fade';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormLabel from '@mui/material/FormLabel';
import RadioGroup from '@mui/material/RadioGroup';
import Radio from '@mui/material/Radio';
import isEmail from 'validator/lib/isEmail';
import ClipLoader from 'react-spinners/ClipLoader';
import CheckIcon from '@mui/icons-material/Check';

const AlertForm = () => {
  const [ serverMessage, setServerMessage ] = useState('');
  const [ showServerMessage, setShowServerMessage ] = useState(false);
  const [ message, setMessage ] = useState('');
  const [ frequency, setFrequency ] = useState('hourly');
  const [ emails, setEmails ] = useState([]);

  const [ messageError, setMessageError ] = useState(false);
  const [ messageHelperText, setMessageHelperText ] = useState('');
  const [ emailsError, setEmailsError ] = useState(false);
  const [ emailsHelperText, setEmailsHelperText ] = useState('');

  const [ hasTested, setHasTested ] = useState('');

  let fadeTestTimeout = useRef(undefined);
  let fadeSaveTimeout = useRef(undefined);
  let fadeNoteTimeout = useRef(undefined);

  const {
    minMessageLength,
    maxMessageLength,
    minEmailsLength,
    maxEmailsLength,
  } = useSelector(state => state.configuration.configuration);

  const dispatch = useDispatch();
  const alertTest = useSelector(state => state.alertTest);
  const alertSave = useSelector(state => state.alertSave);

  const successTestMessage = useMemo(() => {
    return <>
      <CheckIcon color="success" sx={{ marginRight: '8px' }} /> TEST was a success
    </>;
  }, []);

  const successSaveMessage = useMemo(() => {
    return <>
      <CheckIcon color="success" sx={{ marginRight: '8px' }} /> SAVE was a success
    </>;
  }, []);

  const isFormValid = () => {
    let isValid = true;
    if (message.length === 0) {
      isValid = false;
      setMessageError(true);
      setMessageHelperText('required');
    } else if (message.length < minMessageLength) {
      isValid = false;
      setMessageError(true);
      setMessageHelperText(`message must be at least ${minMessageLength} characters in length`);
    } else if (message.length > maxMessageLength) {
      isValid = false;
      setMessageError(true);
      setMessageHelperText(`message must be less than ${maxMessageLength} characters in length`);
    }

    // no validation necessary for frequency,
    // user is given only the two valid options

    if (emails.length === 0) {
      isValid = false;
      setEmailsError(true);
      setEmailsHelperText('required');
    } else if (emails.length < minEmailsLength) {
      isValid = false;
      setEmailsError(true);
      setEmailsHelperText(`provide at least ${minEmailsLength} email address${minEmailsLength > 2 ? 'es' : ''}`);
    } else if (emails.length > maxEmailsLength) {
      isValid = false;
      setEmailsError(true);
      setEmailsHelperText(`provide no more than ${maxEmailsLength} email address${maxEmailsLength > 2 ? 'es' : ''}`);
    }
    return isValid;
  };

  const resetErrors = () => {
    setMessageError(false);
    setMessageHelperText('');
    setEmailsError(false);
    setEmailsHelperText('');
  };

  const handleTestClick = () => {
    // reset errors so that state is sure to change
    resetErrors();
    // wait for errors to reset
    if (fadeTestTimeout.current) {
      clearTimeout(fadeTestTimeout.current);
    }
    fadeTestTimeout.current = setTimeout(() => {
      if (isFormValid()) {
        dispatch(testAlert({
          message,
          frequency,
          emails,
        }));
      }
    }, 0);
  };

  const handleSaveClick = () => {
    // reset errors so that state is sure to change
    resetErrors();
    // wait for errors to reset
    if (fadeSaveTimeout.current) {
      clearTimeout(fadeSaveTimeout.current);
    }
    fadeSaveTimeout.current = setTimeout(() => {
      if (isFormValid()) {
        dispatch(saveAlert({
          message,
          frequency,
          emails,
        }));
      }
    }, 0);
  };

  const resetForm = () => {
    setMessage('');
    setFrequency('hourly');
    setEmails([]);
    setHasTested(false);
  };

  const setNote = useCallback((msg) => {
    setServerMessage(msg);
    setShowServerMessage(true);

    if (fadeNoteTimeout.current) {
      clearTimeout(fadeNoteTimeout.current);
    }
    fadeNoteTimeout.current = setTimeout(() => {
      setShowServerMessage(false);
    }, 3000);
  }, []);

  useEffect(() => {
    return () => {
      if (fadeTestTimeout.current) {
        clearTimeout(fadeTestTimeout.current);
      }
      if (fadeSaveTimeout.current) {
        clearTimeout(fadeSaveTimeout.current);
      }
      if (fadeNoteTimeout.current) {
        clearTimeout(fadeNoteTimeout.current);
      }
    };
  }, []);

  useEffect(() => {
    if ((alertTest.result?.message || alertTest.isError) && !alertTest.isLoading) {
      if (alertTest.isSuccess) {
        setHasTested(true);
        setNote(successTestMessage);
      } else {
        setNote('TEST error');
      }
    }
  }, [alertTest, setNote, successTestMessage]);

  useEffect(() => {
    if ((alertSave.result?.message || alertSave.isError) && !alertSave.isLoading) {
      if (alertSave.isSuccess) {
        setNote(successSaveMessage);
        resetForm();
      } else {
        setNote('SAVE error');
      }
    }
  }, [alertSave, setNote, successSaveMessage]);

  useEffect(() => {
    setHasTested(false);
  }, [message, frequency, emails]);

  return <>
    <InputMinMaxLength
      label="Message"
      placeholder="enter message to send with alert"
      value={message}
      onChange={e => setMessage(e)}
      minValue={minMessageLength}
      maxValue={maxMessageLength}
      error={messageError}
      helperText={messageHelperText}
      required
    />
    <FormControl component="fieldset">
      <FormLabel component="legend" required>Frequency</FormLabel>
      <RadioGroup
        row
        aria-label="frequency"
        name="frequency-radio-group"
        value={frequency}
        onChange={e => setFrequency(e.target.value)}
      >
        <FormControlLabel value="hourly" control={<Radio />} label="Hourly" />
        <FormControlLabel value="daily" control={<Radio />} label="Daily" />
      </RadioGroup>
    </FormControl>
    <InputList
      value={emails}
      onChange={setEmails}
      required
      placeholder="Add emails"
      label="Emails"
      error={emailsError}
      helperText={emailsHelperText}
      validateEntry={isEmail}
      maxItems={maxEmailsLength}
      inputName='email'
      inputPluralName='emails'
    />
    <Box sx={{
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    }}>
      <Box sx={{
        fontSize: 'calc(1rem * (13 / var(--font-base)))',
        lineHeight: '1.75',
        color: theme => theme.palette.primary.note,
        fontWeight: 400,
        marginTop: '22px',
      }}>
        <Fade in={showServerMessage} appear={false} ><Box sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>{serverMessage}</Box></Fade>
      </Box>
      <Box sx={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: '2rem',
        '.MuiButton-root': {
          marginLeft: '24px',
        },
      }}>
        <Button
          variant="contained"
          disabled={alertTest.isLoading || alertSave.isLoading || message === '' || emails.length === 0}
          fullWidth={false}
          onClick={handleTestClick}
          endIcon={alertTest.isLoading ? <ClipLoader color="white" size="16px" /> : null}
        >
          Test
        </Button>
        <Button
          variant="contained"
          disabled={alertTest.isLoading || alertSave.isLoading || !hasTested}
          fullWidth={false}
          onClick={handleSaveClick}
          endIcon={alertSave.isLoading ? <ClipLoader color="white" size="16px" /> : null}
          color="secondary"
        >
          Save
        </Button>
      </Box>
    </Box>
  </>;
};

export default AlertForm;
