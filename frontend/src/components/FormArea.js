import Box from '@mui/material/Box';
import { useSelector } from 'react-redux';

const FormArea = ({ formTitle, formComponent }) => {
  const token = useSelector(state => state.token);
  const configuration = useSelector(state => state.configuration);

  return <>
    <h2 style={{ marginBottom: '24px' }}>{formTitle}</h2>
    { (token.token && configuration.configuration?.minMessageLength) ? (
    <Box sx={{
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'flex-start',
    }}>
      <Box sx={{
        display: 'flex',
        flexDirection: 'column',
        width: '75%',
      }}>
        {formComponent}
      </Box>
    </Box>
    ) : (<>
      { token.isError && <h5>Error: no token</h5> }
      { configuration.isError && <h5>Error: no configuration</h5> }
    </>)}
  </>;
};

export default FormArea;
