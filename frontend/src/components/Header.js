import Box from '@mui/material/Box';
import { Link } from 'react-router-dom';
import logo from '@images/logo.png';

const Header = ({ pageTitle }) => {
  return <Box
    sx={{
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      background: theme => theme.palette.background.header,
      color: theme => theme.palette.text.header,
      width: '100%',
      marginBottom: '48px',
    }}>
      <Box
        component="header"
        className="page-header"
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: '100%',
          maxWidth: 'var(--page-width)',
          padding: '48px 24px',
        }}
      >
        <Link to="/"><img src={logo} alt="LogDNA" height="80" /></Link>
        <h1>{pageTitle}</h1>
    </Box>
  </Box>;
};

export default Header;
