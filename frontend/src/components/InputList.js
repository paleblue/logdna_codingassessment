import { useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import ClearIcon from '@mui/icons-material/Clear';

const InputList = ({
  value = [],
  error = false,
  helperText = ' ',
  inputName = 'input',
  inputPluralName = 'inputs',
  onChange = () => {},
  validateEntry = () => false,
  maxItems = 2,
  allowDuplicates,
  ...rest
}) => {
  const [fieldValue, setFieldValue] = useState(value);
  const [fieldError, setFieldError] = useState(error);
  const [fieldHelperText, setFieldHelperText] = useState(helperText.length === 0 ? ' ' : helperText);
  const [fieldList, setFieldList] = useState(value);
  const [addButtonDisabled, setAddButtonDisabled] = useState(true);

  useEffect(() => {
    setFieldHelperText(helperText.length === 0 ? ' ' : helperText);
  }, [helperText]);

  useEffect(() => {
    setFieldError(error);
  }, [error]);

  useEffect(() => {
    setFieldList(value);
  }, [value]);

  const handleChange = (e) => {
    const val = e.target.value;
    setFieldValue(val);

    if (val.length > 0 && fieldList.length > maxItems - 1) {
      setFieldHelperText(`no more than ${maxItems} ${inputPluralName} allowed`);
      setFieldError(true);

      setAddButtonDisabled(true);
    } else {
      setFieldHelperText(' ');
      setFieldError(false);

      setAddButtonDisabled(!validateEntry(val.trim()));
    }
  };

  const addInput = (val) => {
    setFieldList([ ...fieldList, fieldValue ]);
    onChange([ ...fieldList, fieldValue ]);
  };

  const removeInput = (index) => {
    setFieldList([ ...fieldList.filter((item, i) => i !== index) ]);
    onChange([ ...fieldList.filter((item, i) => i !== index) ]);
  };

  const handleAddInput = () => {
    if (validateEntry(fieldValue.trim())) {
      if (allowDuplicates || !fieldList.includes(fieldValue)) {
        addInput(fieldValue);

        setFieldValue('');
        setAddButtonDisabled(true);
        setFieldHelperText(' ');
        setFieldError(false);
      } else if (!allowDuplicates) {
        setFieldHelperText(`duplicate ${inputName}`);
        setFieldError(true);
      }
    }
  };

  const handleSubmit = (e) => {
    // used to 'listen' for <enter> key
    e.preventDefault();
    handleAddInput();
  };

  return <form onSubmit={handleSubmit}>
    <TextField
      fullWidth
      value={fieldValue}
      variant="outlined"
      error={fieldError}
      helperText={fieldHelperText}
      InputProps={{
        endAdornment: <InputAdornment position="end">
          <IconButton data-testid="add-email-button" disabled={addButtonDisabled} onClick={handleAddInput}>
            <AddCircleIcon />
          </IconButton>
        </InputAdornment>,
      }}
      onChange={handleChange}
      {...rest}
    />
    <Box sx={{
      margin: '0 16px',
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      '.input-list-item': (theme) => ({
        fontSize: 'calc(1rem * (12 / var(--font-base)))',
        lineHeight: 1.75,
        background: theme.palette.background.default,
        borderRadius: '1rem',
        padding: '4px 8px 2px 14px',
        margin: '0 14px 6px 0',
        position: 'relative',
        '.MuiIconButton-root': {
          padding: '3px',
        },
        '.MuiSvgIcon-root': {
          position: 'relative',
          top: '-1px',
        },
      }),
    }}>
      { fieldList.map((item, i) =>
        <div className="input-list-item" key={i}>{item} <IconButton data-testid={`clear-button-${i}`} onClick={e => removeInput(i)}><ClearIcon fontSize="small" /></IconButton></div>,
      )}
    </Box>
  </form>;
};

export default InputList;
