import { useState, useEffect, useCallback } from 'react';
import TextField from '@mui/material/TextField';

const InputMinMaxLength = ({
  value = '',
  minValue = 0,
  maxValue = 200,
  onChange = () => {},
  error = false,
  helperText = ' ',
  ...rest
}) => {
  const [fieldValue, setFieldValue] = useState(value);
  const [fieldError, setFieldError] = useState(error);
  const [fieldHelperText, setFieldHelperText] = useState(helperText.length === 0 ? ' ' : helperText);

  const updateMinMaxText = useCallback((val) => {
    const valLength = val?.length || 0;
    if (valLength > maxValue) {
      setFieldError(true);
      setFieldHelperText(`Exceeded max length of ${maxValue} characters`);
    } else if (valLength > (maxValue * 0.8)) {
      setFieldError(false);
      setFieldHelperText(`${maxValue-valLength} left of ${maxValue}`);
    } else {
      setFieldError(false);
      setFieldHelperText(' ');
    }
  }, [maxValue]);

  useEffect(() => {
    setFieldValue(value);
    updateMinMaxText(value);
  }, [value, updateMinMaxText]);

  useEffect(() => {
    setFieldHelperText(helperText.length === 0 ? ' ' : helperText);
  }, [helperText]);

  useEffect(() => {
    setFieldError(error);
  }, [error]);

  const changeHandler = (e) => {
    const val = e.target.value;
    setFieldValue(val);
    onChange(val.trim());
    updateMinMaxText(val.trim());
  };

  return <TextField
    autoFocus
    multiline
    variant="outlined"
    error={fieldError}
    helperText={fieldHelperText}
    value={fieldValue}
    onChange={changeHandler}
    {...rest}
  />;
};

export default InputMinMaxLength;
