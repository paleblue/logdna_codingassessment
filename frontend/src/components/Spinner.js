import Box from '@mui/material/Box';
import BarLoader from 'react-spinners/BarLoader';

const Spinner = () => {
  return <Box sx={{
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    }}>
    <BarLoader height={4} width={120} color="var(--color-header-background)" />
  </Box>;
};

export default Spinner;
