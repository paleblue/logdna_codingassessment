import { FormArea } from '@components';
import { AlertForm } from '@components';

const AlertManager = () => {
  return <>
    <FormArea formTitle="Email Alert" formComponent={<AlertForm />} />
  </>;
};

export default AlertManager;
