import React from 'react';
import ReactDOM from 'react-dom';
import '@scss/index.scss';
import App from './App';
import { AlertManager } from '@containers';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import rootReducer from '@reducers';
import { composeWithDevTools } from 'redux-devtools-extension';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { ThemeProvider } from  '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { DefaultTheme } from '@themes';

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ThemeProvider theme={DefaultTheme}>
        <CssBaseline />
        <BrowserRouter>
          <Routes>
            <Route path="*" element={<App component={<AlertManager/>} pageTitle="Alert Manager" />} />
          </Routes>
        </BrowserRouter>
      </ThemeProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);
