import { createSlice } from '@reduxjs/toolkit';
import { testAlert, saveAlert } from '@actions';

const initialState = {
  isLoading: false,
  isError: false,
  isSuccess: false,
  errorMessage: '',
  result: {},
};

export const alertTestSlice = createSlice({
  name: 'alertTest',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(testAlert.pending, (state, action) => {
        return {
          isLoading: true,
          isSuccess: false,
          isError: false,
          errorMessage: '',
          result: {},
        };
      })
      .addCase(testAlert.fulfilled, (state, action) => {
        return {
          isLoading: false,
          isSuccess: action.payload.success,
          isError: !action.payload.success,
          errorMessage: action.payload.success ? action.payload.message : '',
          result: { ...action.payload.data },
        };
      })
      .addCase(testAlert.rejected, (state, action) => {
        return {
          isLoading: false,
          isSuccess: false,
          isError: true,
          errorMessage: action.payload || 'test failed',
          result: {},
        };
      });
  },
});

export const alertSaveSlice = createSlice({
  name: 'alertSave',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(saveAlert.pending, (state, action) => {
        return {
          isLoading: true,
          isSuccess: false,
          isError: false,
          errorMessage: '',
          result: {},
        };
      })
      .addCase(saveAlert.fulfilled, (state, action) => {
        return {
          isLoading: false,
          isSuccess: action.payload.success,
          isError: !action.payload.success,
          errorMessage: action.payload.success ? action.payload.message : '',
          result: { ...action.payload.data },
        };
      })
      .addCase(saveAlert.rejected, (state, action) => {
        return {
          isLoading: false,
          isSuccess: false,
          isError: true,
          errorMessage: action.payload || 'save failed',
          result: {},
        };
      });
  },
});
