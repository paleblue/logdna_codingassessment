import { createSlice } from '@reduxjs/toolkit';
import { fetchConfiguration } from '@actions';

const initialState = {
  isLoading: false,
  isError: false,
  errorMessage: '',
  configuration: {
    messageMinLength: 1,
    messageMaxLength: 1000,
    emailsMinLength: 1,
    emailsMaxLength: 10,
  },
};

export const configurationSlice = createSlice({
  name: 'configuration',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchConfiguration.pending, (state, action) => {
        return {
          ...state,
          isLoading: true,
          isError: false,
          errorMessage: '',
          configuration: {},
        };
      })
      .addCase(fetchConfiguration.fulfilled, (state, action) => {
        return {
          isLoading: false,
          isError: false,
          errorMessage: '',
          configuration: { ...action.payload },
        };
      })
      .addCase(fetchConfiguration.rejected, (state, action) => {
        return {
          ...state,
          isLoading: false,
          isError: true,
          errorMessage: 'Failed to get configuration',
          configuration: {},
        };
      });
  },
});
