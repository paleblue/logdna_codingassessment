import { combineReducers } from '@reduxjs/toolkit';

import { alertTestSlice, alertSaveSlice } from './alerts';
import { tokenSlice } from './token';
import { configurationSlice } from './configuration';

export default combineReducers({
  alertTest: alertTestSlice.reducer,
  alertSave: alertSaveSlice.reducer,
  token: tokenSlice.reducer,
  configuration: configurationSlice.reducer,
});
