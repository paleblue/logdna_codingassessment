import { createSlice } from '@reduxjs/toolkit';
import { fetchToken } from '@actions';

const initialState = {
  isLoading: false,
  isError: false,
  errorMessage: '',
  token: '',
};

export const tokenSlice = createSlice({
  name: 'token',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchToken.pending, (state, action) => {
        return {
          ...state,
          isLoading: true,
        };
      })
      .addCase(fetchToken.fulfilled, (state, action) => {
        return {
          isLoading: false,
          isError: false,
          errorMessage: '',
          token: action.payload.token,
        };
      })
      .addCase(fetchToken.rejected, (state, action) => {
        return {
          ...state,
          isLoading: false,
          isError: true,
          errorMessage: 'Failed to get token',
        };
      });
  },
});
